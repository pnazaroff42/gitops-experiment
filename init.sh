terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${GITLAB_PROJ_ID}/terraform/state/${GITLAB_PROJ_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJ_ID}/terraform/state/${GITLAB_PROJ_NAME}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJ_ID}/terraform/state/${GITLAB_PROJ_NAME}/lock" \
    -backend-config="username=${GITLAB_USERNAME}" \
    -backend-config="password=${GITLAB_ACCESS_TOKEN}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
